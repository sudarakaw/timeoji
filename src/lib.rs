// lib.rs:
//
// Copyright 2019, 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
//
// This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
// you are welcome to redistribute it and/or modify it under the terms of the
// BSD 2-clause License. See the LICENSE file for more details.
//

use chrono::prelude::*;
use std::{char, fmt};

#[derive(Debug)]
pub struct ClockFace {
    hour: u32,
    minute: u32,
}

impl<'a> ClockFace {
    pub fn new(hour: u32, minute: u32) -> ClockFace {
        ClockFace { hour, minute }
    }

    pub fn now() -> ClockFace {
        let now = Local::now();
        let (_, hour) = now.hour12();

        ClockFace {
            hour,
            minute: now.minute(),
        }
    }

    fn face_code(&self) -> u32 {
        let one_oclock = 0x1f550; // unicode character '🕐'
        let half_past = 0xc;
        let mut face = one_oclock + self.hour - 1;

        if 15 < self.minute && 45 > self.minute {
            face += half_past
        } else if 45 <= self.minute {
            if 12 > self.hour {
                face += 1
            } else {
                face = one_oclock
            }
        }

        face
    }
}

impl fmt::Display for ClockFace {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", char::from_u32(self.face_code()).unwrap())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::ops::Range;

    fn time_match(hour: u32, minute: u32, face: u32) {
        assert_eq!(
            format!("{}:{} {}", hour, minute, ClockFace::new(hour, minute)),
            format!("{}:{} {}", hour, minute, char::from_u32(face).unwrap())
        );
    }

    mod first_15_minutes {
        use super::*;

        const PAYLOAD: Range<u32> = 0x1f550..0x1f550 + 12;

        #[test]
        fn show_same_on_the_hour() {
            for (idx, face) in PAYLOAD.into_iter().enumerate() {
                for minute in 0..=15 {
                    time_match(1 + idx as u32, minute, face);
                }
            }
        }
    }

    mod minutes_16_to_44 {
        use super::*;

        const PAYLOAD: Range<u32> = 0x1f55c..0x1f55c + 12;

        #[test]
        fn show_same_half_past_the_hour() {
            for (idx, face) in PAYLOAD.into_iter().enumerate() {
                for minute in 16..=44 {
                    time_match(1 + idx as u32, minute, face);
                }
            }
        }
    }

    mod last_15_minutes {
        use super::*;

        const PAYLOAD: Range<u32> = 0x1f550..0x1f550 + 12;

        #[test]
        fn show_next_on_the_hour() {
            let mut one: Vec<u32> = PAYLOAD.collect();
            let mut shifted_hours: Vec<u32> = one.drain(1..).collect();

            shifted_hours.push(one[0]);

            for (idx, face) in shifted_hours.iter().enumerate() {
                for minute in 45..=59 {
                    time_match(1 + idx as u32, minute, *face);
                }
            }
        }
    }
}
